# Makeup Ecommerce based in spree

Spree 3.x ecommerce with custom design, you can use it to check how configure a spree store in the version 3.x

## Features:
  - Mercado Page integration
  - Custom Design without bootstrap
  - Admin section for create banners
  - AJAX Based product slider
  - Responsivity
  - Wishlist list and send email to friend
  - Better search with the word "brand" `search/custom.rb`


## System dependencies:
  - Ubuntu 18.04
  - rbenv
  - MySQL
  - SQLite
  - Ruby version: 2.4.2

## Configuration:
  - Spree standard commands
  - Create Taxonomies:
    - Cuidado de la piel
    - Accesorios
    - Maquillaje
    - Novedades
    - Piel
    - Rostro
    - Labios
    - Pestañas
    - Cejas
    - Ojos

## Bugs and Bullshit
  - Add to cart bug weird
  - Delete item from card dont update price
  - MercadoPago integrate does not work
  - Weirds behaviors very sad :) 
 
 ## Disclaimer
 
 Yes, this project has many bugs. Customizing version 3 is difficult.
 
 <img src="https://user-images.githubusercontent.com/12489333/72689709-17dc5100-3af3-11ea-9cad-64523708fa25.jpg" width="100%"/>
 <img src="https://user-images.githubusercontent.com/12489333/72689719-29255d80-3af3-11ea-892e-234c8e32aebe.jpg" width="100%"/>
 <img src="https://user-images.githubusercontent.com/12489333/72689712-1a3eab00-3af3-11ea-9b6c-ee71b7f021d8.jpg" width="100%"/>
 <img src="https://user-images.githubusercontent.com/12489333/72689714-1c086e80-3af3-11ea-8ce7-6cfa75d296e1.jpg" width="100%"/>
 <img src="https://user-images.githubusercontent.com/12489333/72689713-1ad74180-3af3-11ea-9a07-b88ce484c3f1.jpg" width="100%"/>
 
 
 
