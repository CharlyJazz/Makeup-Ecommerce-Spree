module Spree
  module Admin
    class CmsController < Spree::Admin::BaseController
      def index
        render 'spree/admin/cms/index'
      end

      def banner_top
        @banner_top = BannerTop.find_by id: 1

        render 'spree/admin/cms/cms_banner_top'
      end

      def banner_top_update
        description = params[:banner_top][:description]
        button_text = params[:banner_top][:button_text]
        button_link = params[:banner_top][:button_link]

        @banner_top = BannerTop.find_by id: 1


        if @banner_top
          @banner_top.update(
            description: description,
            button_text: button_text,
            button_link: button_link
          )
        else
          @banner_top.create(
            description: description,
            button_text: button_text,
            button_link: button_link
          )
        end

        flash[:success] = Spree.t('update_banner_first_home')
        redirect_to action: 'banner_top'
      end
    end
  end
end
