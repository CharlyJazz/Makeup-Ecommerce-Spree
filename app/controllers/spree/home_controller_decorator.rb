Spree::HomeController.class_eval do

  def index
    # First Slider
    @slider_piel = Spree::Taxon.where(:name => 'Piel').first
    @slider_piel_products = @slider_piel.products.limit(3).active if @slider_piel
    @slider_rostro = Spree::Taxon.where(:name => 'Rostro').first
    @slider_rostro_products = @slider_rostro.products.limit(3).active if @slider_rostro
    @slider_novedades = Spree::Taxon.where(:name => 'Novedades').first
    @slider_novedades_products = @slider_novedades.products.limit(10).active if @slider_novedades
    @first_banner_home = BannerTop.find(1)
    @taxo_ojos = Spree::Taxon.where(:name => 'Ojos').first
    @products_ojos = @taxo_ojos.products.limit(4).active if @taxo_ojos
  end

  def get_body_taxonomy_results
    taxo = Spree::Taxon.where(:name => params[:taxonomy]).first
    products = taxo.products.limit(4).active if taxo
    render 'spree/shared/_body_taxonomy_results', layout: false, :locals => {:results => products, :results_taxo => taxo}
  end

end
