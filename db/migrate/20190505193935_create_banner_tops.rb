class CreateBannerTops < ActiveRecord::Migration[5.2]
  def change
    create_table :banner_tops do |t|
      t.string :description
      t.string :button_text
      t.string :button_link

      t.timestamps
    end
  end
end
