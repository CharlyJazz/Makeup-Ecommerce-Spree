// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery
//= require jquery_ujs
//= require accounting.min
//= require spree/frontend
//= require_tree .
//= require spree/frontend/spree_auth
//= require spree/frontend/spree_wishlist
//= require toastr

window.showToast = text => {
  const snackbar = document.getElementById("snackbar");

  // Add the "show" class to DIV
  snackbar.className = "show";

  snackbar.innerHTML = text;

  // After 3 seconds, remove the show class from DIV
  setTimeout(function() {
    snackbar.className = snackbar.className.replace("show", "");
  }, 3000);
};

Spree.fetch_recently_viewed_products = productId =>
  $.ajax({
    url: Spree.pathFor("recently_viewed_products?product_id=" + productId),
    success: data => {
      $("#recently_viewed_container").html(data);
    }
  });

document.addEventListener("DOMContentLoaded", () => {
  // Mobile menu animation

  const buttonHeaderMobileOpenFilter = $("#BUTTON-FILTER-HEADER-MOBILE");
  const headerButtonSearch = $("div.Header > div:nth-child(1) > form > button");
  const sidebarMenuComponent = $(".sidebar-menu");
  const toggleButtonMobile = $("#BUTTON-TOGGLE-MOBILE");
  let preventLoop = false;
  headerButtonSearch.click(() => {
    if (preventLoop) return;
    preventLoop = true;
    headerButtonSearch
      .html(
        '<div class="lds-ring"><div></div><div></div><div></div><div></div></div>'
      )
      .trigger("click");
  });
  const hideMenuFilters = () =>
    sidebarMenuComponent.first().css({
      transform: "translateX(0)"
    });

  if (window.innerWidth <= 992) {
    $("input.ButtonRadius.--pink.--shadow.--small").click(function(e) {
      hideMenuFilters();
    });
  }

  toggleButtonMobile.click(() => {
    $("#MENU-MOBILE").toggleClass("--show");
    toggleButtonMobile.toggleClass("--showing");
  });

  buttonHeaderMobileOpenFilter.click(() => {
    hideMenuFilters();
  });

  $("#HIDE-MOBILE-SIDEBAR").click(() => {
    sidebarMenuComponent.first().css({
      transform: "translateX(110%)"
    });
  });

  // -----------------------

  window.listenAddCarButtons = () => {
    $(".click-to-cart").click(function() {
      $(this)
        .children()
        .attr("src", $(this).data("image"));
      const productId = $(this).data("id");
      const headers = {};
      headers["Accept"] = "application/json"; // NO ES OBLIGADO
      headers["Content-Type"] = "application/json"; // OBLIGADO NECESITO PONER ESTO PARA QUE NO SALGA EL 404
      headers["X-Spree-Order-Token"] = SpreeAPI.orderToken;
      headers["Authorization"] = `Bearer ${SpreeAPI.oauthToken}`;
      fetch("http://localhost:3000/api/v2/storefront/cart/add_item", {
        method: "POST",
        headers: headers,
        body: JSON.stringify({
          variant_id: String(productId),
          quantity: 1,
          options: {}
        })
      })
        .then(res => res.json())
        .then(res => {
          console.log(res);
          showToast("Product added to your cart!");
        })
        .catch(err => console.log(err));
    });
  };

  window.listenAddCarButtons();

  window.deleteCardItem = idItem => {
    const headers = {};
    headers["Accept"] = "application/json"; // NO ES OBLIGADO
    headers["Content-Type"] = "application/json"; // OBLIGADO NECESITO PONER ESTO PARA QUE NO SALGA EL 404
    headers["X-Spree-Order-Token"] = SpreeAPI.orderToken;
    headers["Authorization"] = `Bearer ${SpreeAPI.oauthToken}`;
    fetch(
      `http://localhost:3000/api/v2/storefront/cart/remove_line_item/${idItem}`,
      {
        method: "DELETE",
        headers: headers
      }
    )
      .then(res => res.json())
      .then(res => {
        console.log(res);
        $(`.line-item[data-id='${idItem}']`).remove();
        showToast("Item deleted from the cart!");
      })
      .catch(err => console.log(err));
  };

  $("#MANU_BAR").click(() => {
    $("#MENU-NAVEGATION").toggleClass("--active");
  });
});
