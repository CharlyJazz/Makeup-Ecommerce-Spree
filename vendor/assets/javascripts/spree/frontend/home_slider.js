document.addEventListener("DOMContentLoaded", () => {
  // ----------------------------------------------- TAXO BODY LOGIC
  let loadingTaxoBody = false;
  $(".fetchTaxoBody").click(e => {
    // prevent troll people
    if (loadingTaxoBody) return false;
    loadingTaxoBody = true;
    $(".fetchTaxoBody.--normal")
      .removeClass("--normal")
      .addClass("--noactive");
    e.target.classList.add("--normal");
    e.target.classList.remove("--noactive");
    $("#productsBodyTaxo > div").addClass("out");
    setTimeout(() => {
      $("#productsBodyTaxo")
        .empty()
        .css({ height: "820px" })
        .append(
          '<div class="loader-wrapper"><div class="loader"></div><p>Loading Products...</p></div>'
        );
      $(".see-more-wrapper").hide();
      const taxonomyName = e.target.dataset.taxo;
      $.ajax({
        url: Spree.pathFor(
          `get_body_taxonomy_results?taxonomy=${taxonomyName}`
        ),
        success: data => {
          $("#productsBodyTaxo")
            .empty()
            .css({ height: "auto" })
            .append(data)
            .fadeIn("slow");
          const idTaxo = $("#results-taxo-body-wrapper").data()["href_taxo"];
          $(".see-more-wrapper")
            .children()[0]
            .setAttribute("href", idTaxo);
          $(".see-more-wrapper").show();
          // ADD CAR BUTTON CHECK IF IS AVAILABLE
          if (SpreeAPI.orderToken) {
            // Sin order token se va a la mierda todo.
            [...$(".click-to-cart")].forEach(button => {
              $(button).prop("hidden", false);
            });
          }
          loadingTaxoBody = false;
          window.listenAddCarButtons();
        },
        error: () => {
          loadingTaxoBody = false;
        }
      });
    }, 450);
  });

  // ----------------------------------------------- 3 ITEMS SLIDER

  const idSlider3Items = document.getElementById("SLIDER_HOME_3_ITEMS");

  if (idSlider3Items) {
    let currentIndex = 1;
    setInterval(function() {
      document
        .getElementsByClassName("dot")
        [currentIndex > 1 ? currentIndex - 1 : 0].classList.remove("--active");
      const currentSlideItem = document.getElementsByClassName(
        `ITEM-${currentIndex}`
      )[0];
      currentSlideItem.setAttribute("hidden", true);
      currentSlideItem.classList.remove("--active");
      currentIndex = currentIndex === 3 ? 1 : currentIndex + 1;
      const slideItem = document.getElementsByClassName(
        `ITEM-${currentIndex}`
      )[0];
      slideItem.setAttribute("hidden", false);
      slideItem.classList.add("--active");
      slideItem.classList.add("--enter");
      document
        .getElementsByClassName("dot")
        [currentIndex - 1].classList.add("--active");
    }, 3500);
  }

  /* - - - - - - - - - - - - - - - -  - - - - - - - - */
  /* Others Slider Logic */
  const idSlider = document.getElementById("SLIDER_HOME_NEWS_PRODUCTS");

  if (idSlider) {
    const tns = window.tns;

    // Calcular el valor de items dependiendo de la resolucion
    // Cada item tiene 218px de width
    const markupPrev = `
      <svg width="11" height="20" viewBox="0 0 11 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M10.6205 0.990059C10.1305 0.500059 9.34055 0.500059 8.85055 0.990059L0.540547 9.30006C0.150547 9.69006 0.150547 10.3201 0.540547 10.7101L8.85055 19.0201C9.34055 19.5101 10.1305 19.5101 10.6205 19.0201C11.1105 18.5301 11.1105 17.7401 10.6205 17.2501L3.38055 10.0001L10.6305 2.75006C11.1105 2.27006 11.1105 1.47006 10.6205 0.990059Z" fill="white"/>
      </svg>
    `;
    const markupNext = `
      <svg width="11" height="20" viewBox="0 0 11 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M0.379453 19.0099C0.869452 19.4999 1.65945 19.4999 2.14945 19.0099L10.4595 10.6999C10.8495 10.3099 10.8495 9.67994 10.4595 9.28994L2.14945 0.979941C1.65945 0.489941 0.869451 0.489942 0.379451 0.979941C-0.110549 1.46994 -0.110549 2.25994 0.379451 2.74994L7.61945 9.99994L0.369454 17.2499C-0.110545 17.7299 -0.110547 18.5299 0.379453 19.0099Z" fill="white"/>
      </svg>
    `;

    if (window.innerWidth < 820) {
      [...$("#SLIDER_HOME_NEWS_PRODUCTS").find(".ProductPreviewCard")].forEach(
        n => console.log($(n).css({
          margin: "auto"
        }))
      );
    }

    tns({
      container: idSlider,
      slideBy: "page",
      speed: 400,
      nav: false,
      controlsText: [markupPrev, markupNext],
      items: 1,
      center: true,
      responsive: {
        480: {
          items: 2
        },
        820: {
          items: 3
        },
        1100: {
          items: 4
        },
        1600: {
          items: 6
        }
      }
    });
  }
});
