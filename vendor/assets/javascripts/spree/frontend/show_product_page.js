document.addEventListener("DOMContentLoaded", () => {
  const markupPrev = `
  <svg width="11" height="20" viewBox="0 0 11 20" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M10.6205 0.990059C10.1305 0.500059 9.34055 0.500059 8.85055 0.990059L0.540547 9.30006C0.150547 9.69006 0.150547 10.3201 0.540547 10.7101L8.85055 19.0201C9.34055 19.5101 10.1305 19.5101 10.6205 19.0201C11.1105 18.5301 11.1105 17.7401 10.6205 17.2501L3.38055 10.0001L10.6305 2.75006C11.1105 2.27006 11.1105 1.47006 10.6205 0.990059Z" fill="white"/>
  </svg>
`;
  const markupNext = `
  <svg width="11" height="20" viewBox="0 0 11 20" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M0.379453 19.0099C0.869452 19.4999 1.65945 19.4999 2.14945 19.0099L10.4595 10.6999C10.8495 10.3099 10.8495 9.67994 10.4595 9.28994L2.14945 0.979941C1.65945 0.489941 0.869451 0.489942 0.379451 0.979941C-0.110549 1.46994 -0.110549 2.25994 0.379451 2.74994L7.61945 9.99994L0.369454 17.2499C-0.110545 17.7299 -0.110547 18.5299 0.379453 19.0099Z" fill="white"/>
  </svg>
`;

  const SLIDER_PRODUCT_IMAGES_WRAPPER = document.getElementById(
    "SLIDER_PRODUCT_IMAGES_WRAPPER"
  );

  if (window.innerWidth <= 1340 && SLIDER_PRODUCT_IMAGES_WRAPPER) {
  }

  const createSlider = (array, removeLoaded, currentSliderRendered) => {
    if (!array.length) {
      return;
    }

    const divSliderString = `
    <div class="customize" id="SLIDER_PRODUCT_IMAGES">
      ${array.map(imgObj => {
        return `
          <div class='customize-slider-item'>
            <img src="${imgObj.slider}">
          </div>
        `;
      })}
    </div>`;

    const divThumbnailString = `
    <div class="customize-tools">
      <ul class="thumbnails" id="customize-thumbnails">
        ${array
          .map(imgObj => {
            return `<li><img src="${imgObj.thumbnail}"></li>`;
          })
          .join(" ")}
      </ul>
    </div>`;

    if (removeLoaded) {
      $("#LOADER").remove();
    }

    if (currentSliderRendered) {
      currentSliderRendered.destroy();
      [...$("#SLIDER_PRODUCT_IMAGES_WRAPPER").children()].forEach(c =>
        c.remove()
      );
    }

    $("#SLIDER_PRODUCT_IMAGES_WRAPPER").append(
      `${divSliderString}${divThumbnailString}`
    );

    const sliderOptions = {
      container: "#SLIDER_PRODUCT_IMAGES",
      items: 1,
      controlsText: [markupPrev, markupNext],
      navContainer: "#customize-thumbnails",
      navAsThumbnails: true,
      swipeAngle: false,
      speed: 400,
      center: true
    };

    return tns(sliderOptions);
  };

  if (SLIDER_PRODUCT_IMAGES_WRAPPER) {
    const productId = $("#LOADER").data().productId;
    fetch(
      `http://localhost:3000/api/v2/storefront/products/${productId}?include=default_variant,variants,option_types,product_properties,taxons,images`,
      {
        method: "GET",
        headers: {
          Accept: "application/json"
        }
      }
    )
      .then(res => res.json())
      .then(res => {
        const imagesObj = [
          // { thumbnail: '', slider: ''}
        ];
        if ("data" in res && "included" in res && res.included.length) {
          const imagesIncludeds = res.included.filter(n => n.type === "image");
          const lengthImagesIncludeds = imagesIncludeds.length;
          if (lengthImagesIncludeds) {
            for (let i = 0; i < lengthImagesIncludeds; i++) {
              const image = imagesIncludeds[i];
              const variant_id = image.attributes.viewable_id;
              const thumbnail = image.attributes.styles.filter(i =>
                window.innerWidth > 991 ? i.width === "100" : i.width === "100"
              )[0].url;
              const slider = image.attributes.styles.filter(i =>
                window.innerWidth > 991 ? i.width === "600" : i.width === "240"
              )[0].url;
              imagesObj.push({ thumbnail, slider, variant_id });
            }
          }
        }

        let currentSlider = createSlider(imagesObj, true, null);

        $('#product-variants input[type="radio"]').click(event => {
          const onlyActiveVariantIdImages = imagesObj.filter(
            ({ variant_id }) => variant_id == event.target.value
          );
          $($(".product-price-value")[0]).text($(event.target).data("price"));
          if (onlyActiveVariantIdImages.length) {
            currentSlider = createSlider(
              onlyActiveVariantIdImages,
              false,
              currentSlider
            );
          }
        });
      })
      .catch(err => console.log(err));
  }
});
